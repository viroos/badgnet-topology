# Topology Detector for Flow3r Badges

## Overview

 The application works by detecting the position/sequence of a badge in a chain of connected badges using audio jack cables and communicating using IPv6 over the audio jacks (aka badge-net).

## Functionality

The application automatically adjusts to the changes in the badge chain and reflects the position update on the badge display. In the absence of any detected connection, it will display "No connection".

## Requirements

- The application is designed to run on a Flow3r Badge.

## Usage

To run the application:

1. Upload the app onto the Flow3r Badge
2. Connect the Flow3r Badge to another badge via an audio jack cable (always connect right jack to the left jack on the other badge)
3. The application should run and display the position of the badge in the chain on the badge display

## Code Explanation

The code executes the following steps:

1. Configures both left and right audio jacks
2. Creates a socket and binds it to a certain address and port
3. Registers this socket to the poll object for monitoring
4. In the `draw()` method, it clears the screen and displays the badge position in the chain
5. In the `think()` method, the application enables left and right links if not already enabled, calculates the time since last broadcast, and receives data from other badges

## License

This project is released under the MIT License 

## Attribution

Special thanks to Mewp whose code in the project Gard3n was used as reference for parts of this project.

## Contacts
I'm viroos. You can find me on #hswaw channel irc.libera.chat. 
Constructive feedback, contributions, and suggestions are highly welcomed!
